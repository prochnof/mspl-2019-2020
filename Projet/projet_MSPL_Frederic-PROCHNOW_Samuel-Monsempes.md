---
title: "Le chomage en France"
author: "Frederic PROCHNOW  - Samuel Monsempes"
output: 
  html_document: 
    keep_md: yes
    highlight: zenburn
    toc: yes
  pdf_document: 
    toc: yes
---

# Preparation du projet

## Configuration et librairie


```r
#install.packages('knitr', dependencies = TRUE)
#install.packages("rmarkdown")
#install.packages("ggplot2")
#install.packages("cowplot")
```



## Recuperation des donnees :

Nous sommes parti des donnees officiel de l'annee 2018 issue du site de l'INSEE. Les données de l'année 2019 n'etant pas encore disponible.
(https://www.insee.fr/fr/statistiques).


```r
library(readxl)

demandeur_emploi_AN_2018 <- read_excel("demandeur-emploi_AN_2018.xls", 
    sheet = "QP", skip = 5)
```

---

# Les Femmes sont elles plus touchee par le chomage que les hommes en 2018 ?
(Quels sont les populations les plus touche par le chomage ?)

Nous repondrons a cette question à travers ce document.

## Au niveau national ?

### Donees


```r
#FEMMES

#on recupere l'ensemble des chomeurs
chomeur_national_femme <- demandeur_emploi_AN_2018$ABCDE_F
#(on enleve les valeurs NA par 0)
chomeur_national_femme[is.na(chomeur_national_femme)] <- 0
#nombre total de chomeur
nombre_chomeur_femme <- sum(chomeur_national_femme)
remove(chomeur_national_femme)

# HOMMES

#on recupere l'ensemble des chomeurs
chomeur_national_homme <- demandeur_emploi_AN_2018$ABCDE_H
#(on enleve les valeurs NA par 0)
chomeur_national_homme[is.na(chomeur_national_homme)] <- 0
#nombre total de chomeur
nombre_chomeur_homme <- sum(chomeur_national_homme)
remove(chomeur_national_homme)

# TOTAL HOMMES ET FEMMES

total_chomeur_national<-nombre_chomeur_homme+nombre_chomeur_femme

# POURCENTAGE

pourcentage_homme_national <- (nombre_chomeur_homme/total_chomeur_national) * 100
pourcentage_femme_national <- (nombre_chomeur_femme/total_chomeur_national) * 100

data_chomeur_national <- data.frame(
  Genre = c("Homme", "Femme"),
  Nombre = c(nombre_chomeur_homme, nombre_chomeur_femme),
  Pourcentage = c(pourcentage_homme_national, pourcentage_femme_national)
)

remove(nombre_chomeur_femme)
remove(nombre_chomeur_homme)

print(data_chomeur_national)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 913581    53.29598
## 2 Femme 800584    46.70402
```

```r
print(total_chomeur_national)
```

```
## [1] 1714165
```
Nous avons donc **913581** d'hommes pour **800584** de femmes et un total de **1714165**.
Cela nous donne **53.29598 %** d'hommes et **46.70402 %** de femmes.

### Representation graphique

Nous faisons ainsi une representation graphique, pour que cela soit plus visuelle :

```r
library('ggplot2')

graph_National<- ggplot(data_chomeur_national, aes(x="Pourcentage", y=Pourcentage, fill=Genre))+
  geom_bar(width = 100, stat = "identity")+
  coord_polar("y", start=0)+
  scale_fill_brewer(palette="Blues")+
  theme_minimal()

graph_National
```

![](projet_MSPL_Frederic-PROCHNOW_Samuel-Monsempes_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

### Conclusion

Il y a donc plus d'hommes que de femmes au chomage.

## Le chomage au regard de l'age

### Donnees des moins de 26 ans

Pour les moins de 26 ans :

```r
chomeur_moins26_homme <- demandeur_emploi_AN_2018$ABC_25_H
chomeur_moins26_homme[is.na(chomeur_moins26_homme)] <- 0
nombre_chomeur_moins26_homme <- sum(chomeur_moins26_homme)
remove(chomeur_moins26_homme)

chomeur_moins26_femme <- demandeur_emploi_AN_2018$ABC_25_F
chomeur_moins26_femme[is.na(chomeur_moins26_femme)] <- 0
nombre_chomeur_moins26_femme <- sum(chomeur_moins26_femme)
remove(chomeur_moins26_femme)

nombre_chomeur_moins26_total <- nombre_chomeur_moins26_homme+nombre_chomeur_moins26_femme

pourcentage_chomeur_moins26_homme <- nombre_chomeur_moins26_homme/nombre_chomeur_moins26_total * 100
pourcentage_chomeur_moins26_femme <- nombre_chomeur_moins26_femme/nombre_chomeur_moins26_total * 100

data_chomeur_moins26 <- data.frame(
  Genre = c("Homme", "Femme"),
  Nombre = c(nombre_chomeur_moins26_homme, nombre_chomeur_moins26_femme),
  Pourcentage = c(pourcentage_chomeur_moins26_homme,pourcentage_chomeur_moins26_femme)
)

remove(nombre_chomeur_moins26_homme)
remove(nombre_chomeur_moins26_femme)

print(data_chomeur_moins26)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 118736    50.68189
## 2 Femme 115541    49.31811
```

```r
print(nombre_chomeur_moins26_total)
```

```
## [1] 234277
```
Nous avons donc quaisment le même nombre d'hommes et de femmes. Femmes qui pesent legerement plus sur la balance.

### Donnees des 26 a 50 ans

Pour les personnes de 26 a 50 ans  :

```r
chomeur_26_50_homme <- demandeur_emploi_AN_2018$ABC_2649_H
chomeur_26_50_homme[is.na(chomeur_26_50_homme)] <- 0
nombre_chomeur_26_50_homme <- sum(chomeur_26_50_homme)
remove(chomeur_26_50_homme)

chomeur_26_50_femme <- demandeur_emploi_AN_2018$ABC_2649_F
chomeur_26_50_femme[is.na(chomeur_26_50_femme)] <- 0
nombre_chomeur_26_50_femme <- sum(chomeur_26_50_femme)
remove(chomeur_26_50_femme)

nombre_chomeur_26_50_total <- nombre_chomeur_26_50_femme+nombre_chomeur_26_50_homme

pourcentage_chomeur_26_50_homme <- nombre_chomeur_26_50_homme/nombre_chomeur_26_50_total * 100
pourcentage_chomeur_26_50_femme <- nombre_chomeur_26_50_femme/nombre_chomeur_26_50_total * 100

data_chomeur_26_50 <- data.frame(
  Genre = c("Homme", "Femme"),
  Nombre = c(nombre_chomeur_26_50_homme, nombre_chomeur_26_50_femme),
  Pourcentage = c(pourcentage_chomeur_26_50_homme,pourcentage_chomeur_26_50_femme)
)

remove(nombre_chomeur_26_50_homme)
remove(nombre_chomeur_26_50_femme)

print(data_chomeur_26_50)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 507747    53.62973
## 2 Femme 439017    46.37027
```

```r
print(nombre_chomeur_26_50_total)
```

```
## [1] 946764
```
La différence entre le nombre d'hommes et de femmes est plus marqué que pour les moins de 26 ans et rapproche plus de la moyenne national.

### Donnees des plus de 50 ans

Nous avons enfin les plus de 50 ans :

```r
chomeur_plus50_homme <- demandeur_emploi_AN_2018$ABC_50_H
chomeur_plus50_homme[is.na(chomeur_plus50_homme)] <- 0
nombre_chomeur_plus50_homme <- sum(chomeur_plus50_homme)
remove(chomeur_plus50_homme)

chomeur_plus50_femme <- demandeur_emploi_AN_2018$ABC_50_F
chomeur_plus50_femme[is.na(chomeur_plus50_femme)] <- 0
nombre_chomeur_plus50_femme <- sum(chomeur_plus50_femme)
remove(chomeur_plus50_femme)

nombre_chomeur_plus50_total <- nombre_chomeur_plus50_homme+nombre_chomeur_plus50_femme

pourcentage_chomeur_plus50_homme <- nombre_chomeur_plus50_homme/nombre_chomeur_plus50_total * 100
pourcentage_chomeur_plus50_femme <- nombre_chomeur_plus50_femme/nombre_chomeur_plus50_total * 100

data_chomeur_plus50 <- data.frame(
  Genre = c("Homme", "Femme"),
  Nombre = c(nombre_chomeur_plus50_homme, nombre_chomeur_plus50_femme),
  Pourcentage = c(pourcentage_chomeur_plus50_homme,pourcentage_chomeur_plus50_femme)
)

remove(nombre_chomeur_plus50_homme)
remove(nombre_chomeur_plus50_femme)

print(data_chomeur_plus50)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 203829    54.53779
## 2 Femme 169910    45.46221
```

```r
print(nombre_chomeur_plus50_total)
```

```
## [1] 373739
```
La difference est encore plus forte que pour les 26 - 50 ans.

### Donnees global


```r
remove(demandeur_emploi_AN_2018)

data_chomeur_par_age_et_par_sexe <- 
  as.matrix(data.frame(Moins_26_ans = c(pourcentage_chomeur_moins26_homme, pourcentage_chomeur_moins26_femme),
                             De_26ans_a_50ans = c(pourcentage_chomeur_26_50_homme, pourcentage_chomeur_26_50_femme),
                             Plus_26_ans = c(pourcentage_chomeur_plus50_homme, pourcentage_chomeur_plus50_femme)))
rownames(data_chomeur_par_age_et_par_sexe) <- c("Hommes", "Femmes")

print(data_chomeur_par_age_et_par_sexe)
```

```
##        Moins_26_ans De_26ans_a_50ans Plus_26_ans
## Hommes     50.68189         53.62973    54.53779
## Femmes     49.31811         46.37027    45.46221
```

### Représentations graphiques


```r
library('ggplot2')
library('cowplot')

graph_moins26<- ggplot(data_chomeur_moins26, aes(x="Pourcentage", y=Pourcentage, fill=Genre))+
  geom_bar(width = 100, stat = "identity")+
  coord_polar("y", start=0)+
  scale_fill_brewer(palette="Blues")+
  theme_minimal()

graph_26_50<- ggplot(data_chomeur_26_50, aes(x="Pourcentage", y=Pourcentage, fill=Genre))+
  geom_bar(width = 100, stat = "identity")+
  coord_polar("y", start=0)+
  scale_fill_brewer(palette="Blues")+
  theme_minimal()

graph_plus50<- ggplot(data_chomeur_plus50, aes(x="Pourcentage", y=Pourcentage, fill=Genre))+
  geom_bar(width = 100, stat = "identity")+
  coord_polar("y", start=0)+
  scale_fill_brewer(palette="Blues")+
  theme_minimal()

plot_grid(graph_moins26, graph_26_50, labels=c("Moins de 26 ans", "De 26 ans à 50 ans"), ncol = 2, nrow = 1)
```

![](projet_MSPL_Frederic-PROCHNOW_Samuel-Monsempes_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

```r
plot_grid(graph_plus50, labels=c("Plus de 50 ans"), ncol = 2, nrow = 1)
```

![](projet_MSPL_Frederic-PROCHNOW_Samuel-Monsempes_files/figure-html/unnamed-chunk-9-2.png)<!-- -->
Ou vue autrement

```r
barplot(data_chomeur_par_age_et_par_sexe,col = c("Blue", "Red"),beside = T)
legend("bottomright",legend = c("Hommes", "Femmes"),fill = c("Blue", "Red"))
```

![](projet_MSPL_Frederic-PROCHNOW_Samuel-Monsempes_files/figure-html/unnamed-chunk-10-1.png)<!-- -->
Ce graphe est flagrant, plus la personne est agee, plus écart homme-femme se creuse.

### Graphique de répartiton global


```r
nombre_total_chomeur_ABC <- nombre_chomeur_plus50_total+nombre_chomeur_26_50_total+nombre_chomeur_moins26_total

pourcentage_chomeur_moins26_total <- nombre_chomeur_moins26_total/nombre_total_chomeur_ABC * 100
pourcentage_chomeur_26_50_total <- nombre_chomeur_26_50_total/nombre_total_chomeur_ABC * 100
pourcentage_chomeur_plus50_total <- nombre_chomeur_plus50_total/nombre_total_chomeur_ABC * 100

data_chomeur_par_age <- data.frame(
  Age= c("moins 26 ans","26 ans a 50 ans","plus de 50 ans"),
  Nombre = c(pourcentage_chomeur_moins26_total, pourcentage_chomeur_26_50_total, pourcentage_chomeur_plus50_total)
  )
print(data_chomeur_par_age)
```

```
##               Age   Nombre
## 1    moins 26 ans 15.06818
## 2 26 ans a 50 ans 60.89376
## 3  plus de 50 ans 24.03806
```

Et ainsi construire ce graphique :

```r
library('ggplot2')
graph2<- ggplot(data_chomeur_par_age, aes(x="Pourcentage", y=Nombre, fill=Age))+
geom_bar(width = 100, stat = "identity")+coord_polar("y", start=0)+
  scale_fill_brewer(palette="Blues")+
  scale_fill_brewer(palette="Paired")+
  geom_text(aes(y=Nombre, label=Nombre), vjust=2, 
            color="white", size=3)+
  theme_minimal()
graph2
```

![](projet_MSPL_Frederic-PROCHNOW_Samuel-Monsempes_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

# Conclusion 

On remarque que la majeur partie des chomeurs ont entre 26 et 50 ans.
Il y a une chose aussi plus intéressante, l'écart entre le nombre d'hommes et de femmes se creuse plus que l'on avance dans le temps. Il semble que les hommes est une plus grande difficultes a trouver du travail plus leurs ages grandit. Ce qui n'est pas ilogique et se confirme avec les tendances tout age confondus au niveau national.
Toutefois la parité est quasi respecté chez les jeunes (moins de 26 ans).
Cela confirme egalement qu'il y a un lien de coreleation entre l age et la parite dans les demandeurs d'emploi.

Concernant la question, Les Femmes sont elles plus touchee par le chomage que les hommes en 2018 ?
La reponse est simple : NON, ce sont les hommes et le cela est de plus en plus flagrant en fonction de leurs ages.

# Ouverture : Et l'avenir ?

Il peut etre interessant de comparer avec les autres annees pour ce donner une idée de la tendance.


```r
library(readxl)

demandeur_emploi_AN_2017 <- read_excel("demandeur-emploi_AN_2017.xls", 
    sheet = "QP", skip = 5)

demandeur_emploi_AN_2016 <- read_excel("demandeur-emploi_AN_2016.xls", 
    sheet = "QP", skip = 5)

#FEMMES

#on recupere l'ensemble des chomeurs
chomeur_national_femme <- demandeur_emploi_AN_2016$ABCDE_F
#(on enleve les valeurs NA par 0)
chomeur_national_femme[is.na(chomeur_national_femme)] <- 0
#nombre total de chomeur
nombre_chomeur_femme_2016 <- sum(chomeur_national_femme)
remove(chomeur_national_femme)

# HOMMES

#on recupere l'ensemble des chomeurs
chomeur_national_homme <- demandeur_emploi_AN_2016$ABCDE_H
#(on enleve les valeurs NA par 0)
chomeur_national_homme[is.na(chomeur_national_homme)] <- 0
#nombre total de chomeur
nombre_chomeur_homme_2016 <- sum(chomeur_national_homme)
remove(chomeur_national_homme)

# TOTAL HOMMES ET FEMMES

total_chomeur_national_2016 <-nombre_chomeur_homme_2016+nombre_chomeur_femme_2016

# POURCENTAGE

pourcentage_homme_national_2016 <- (nombre_chomeur_homme_2016/total_chomeur_national_2016) * 100
pourcentage_femme_national_2016 <- (nombre_chomeur_femme_2016/total_chomeur_national_2016) * 100

data_chomeur_national_2016 <- data.frame(
  Genre = c("Homme", "Femme"),
  Nombre = c(nombre_chomeur_homme_2016, nombre_chomeur_femme_2016),
  Pourcentage = c(pourcentage_homme_national_2016, pourcentage_femme_national_2016)
)

remove(nombre_chomeur_femme_2016)
remove(nombre_chomeur_homme_2016)

#FEMMES

#on recupere l'ensemble des chomeurs
chomeur_national_femme <- demandeur_emploi_AN_2017$ABCDE_F
#(on enleve les valeurs NA par 0)
chomeur_national_femme[is.na(chomeur_national_femme)] <- 0
#nombre total de chomeur
nombre_chomeur_femme_2017 <- sum(chomeur_national_femme)
remove(chomeur_national_femme)

# HOMMES

#on recupere l'ensemble des chomeurs
chomeur_national_homme <- demandeur_emploi_AN_2017$ABCDE_H
#(on enleve les valeurs NA par 0)
chomeur_national_homme[is.na(chomeur_national_homme)] <- 0
#nombre total de chomeur
nombre_chomeur_homme_2017 <- sum(chomeur_national_homme)
remove(chomeur_national_homme)

# TOTAL HOMMES ET FEMMES

total_chomeur_national_2017 <-nombre_chomeur_homme_2017+nombre_chomeur_femme_2017

# POURCENTAGE

pourcentage_homme_national_2017 <- (nombre_chomeur_homme_2017/total_chomeur_national_2017) * 100
pourcentage_femme_national_2017 <- (nombre_chomeur_femme_2017/total_chomeur_national_2017) * 100

data_chomeur_national_2017 <- data.frame(
  Genre = c("Homme", "Femme"),
  Nombre = c(nombre_chomeur_homme_2017, nombre_chomeur_femme_2017),
  Pourcentage = c(pourcentage_homme_national_2017, pourcentage_femme_national_2017)
)

remove(nombre_chomeur_femme_2017)
remove(nombre_chomeur_homme_2017)
```

En 2016 :

```r
print(data_chomeur_national_2016)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 918738     54.0812
## 2 Femme 780074     45.9188
```

En 2017 :

```r
print(data_chomeur_national_2017)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 930402    53.48638
## 2 Femme 809110    46.51362
```

En 2018 :

```r
print(data_chomeur_national)
```

```
##   Genre Nombre Pourcentage
## 1 Homme 913581    53.29598
## 2 Femme 800584    46.70402
```

La tendance montre un écart grandisant entre le nombre d'hommes et de femmes et tend vers l'hypothese que cette ecart va encore grandir dans les annees futur.
Cette ecart est egalement un exemple illustrant la conclusion de ce document.

Que nous reserve l'avenir ?
